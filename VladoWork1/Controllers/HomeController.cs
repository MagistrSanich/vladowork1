﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VladoWork1.Models;

namespace VladoWork1.Controllers
{
    public class HomeController : Controller
    {
        AppDbContext db;
        public HomeController( AppDbContext appDbContext)
        {
            db = appDbContext;

            DatabaseInit();
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        private void DatabaseInit()
        {
            if (db.Roles.Any()) return;
            string ADMIN_ROLE_NAME = "admin";
            string USER_ROLE_NAME = "user";

            string ADMIN_EMAIL = "admin@gmail.com";
            string ADMIN_PAS = "P_assword1";

            Role adminRole = new Role { Name = ADMIN_ROLE_NAME };
            Role userRole = new Role { Name = USER_ROLE_NAME };

            db.Roles.Add(userRole);
            db.Roles.Add(adminRole);


            db.Users.Add(new Models.User { Email = ADMIN_EMAIL, Password = ADMIN_PAS, Role = adminRole });

            db.SaveChanges();
        }

    }
}
